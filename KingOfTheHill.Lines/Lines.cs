﻿using KingOfTheHill.Lines.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KingOfTheHill.Lines
{
    public class Lines
    {
        public static IEnumerable<GameViewModel> GetLines()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            var client = new WebClient();
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            var xmlData = client.DownloadString(string.Format("https://www.sportsbook.ag/rss/ncaa-basketball?x={0}", Guid.NewGuid().ToString()));


            XDocument xml = XDocument.Parse(xmlData);
            var games = xml.Descendants("channel").Descendants("item");
            return games.Select(g => getModel(g.Element("description").Value)).ToList();
        }

        private class oddsDataExtract
        {
            public string Information { get; set; }
            public int LastPosition { get; set; }
        }
        private static GameViewModel getModel(string rssDescription)
        {

            var teamOneData = GetOddsData(rssDescription, "Bet on ", "<", 0);
            var spreadData = GetOddsData(rssDescription, ">", "(", teamOneData.LastPosition);
            var teamTwoData = GetOddsData(rssDescription, "or ", "<", spreadData.LastPosition);

            return new GameViewModel
            {
                TeamOne = teamOneData.Information,
                TeamTwo = teamTwoData.Information,
                Line = decimal.Parse(spreadData.Information)
            };
        }

        private static oddsDataExtract GetOddsData(String rssDescription, string startString, string endString, int startPosition)
        {
            var betEndPosition = rssDescription.IndexOf(startString, startPosition) + startString.Length;
            if (betEndPosition > 0)
            {
                //-1 for the space
                return new oddsDataExtract { Information = rssDescription.Substring(betEndPosition, rssDescription.IndexOf(endString, betEndPosition) - betEndPosition - 1), LastPosition = rssDescription.IndexOf(endString, betEndPosition) };
            }
            return new oddsDataExtract { };
        }
    }
}
