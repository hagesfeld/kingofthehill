﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Lines.Models.View
{
    public class GameViewModel
    {
        public string TeamOne { get; set; }
        public string TeamTwo { get; set; }
        public decimal Line { get; set; }
    }
}
