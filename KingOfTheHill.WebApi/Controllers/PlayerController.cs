﻿using KingOfTheHill.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace KingOfTheHill.WebApi.Controllers
{
    public class PlayerController : ApiController
    {
        private Player _player = new Player();
        // GET: api/Player
        public IEnumerable<Player> Get()
        {
            return _player.GetAll();
        }

        // GET: api/Player/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Player
        public void Post([FromBody]string name)
        {
            _player.Add(name);
        }

        // PUT: api/Player/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Player/5
        public void Delete(int id)
        {
        }
    }
}
