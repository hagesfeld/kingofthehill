﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Data.Models
{
    public partial class PlayerTournamentTeam
    {
        public ObjectId Id{ get; set; }
        public Player Player { get; set; }
        public TournamentTeam TournamentTeam { get; set; }
        public bool IsActive { get; set; }
    }
}
