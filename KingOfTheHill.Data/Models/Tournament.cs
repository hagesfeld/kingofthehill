﻿using KingOfTheHill.Data.Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Data
{
    public partial class Tournament
    {
        public ObjectId Id { get; set; } = new ObjectId();
        public int Year { get; set; }
        public virtual List<TournamentTeam> TourneyTeams { get; set; }
    }
}
