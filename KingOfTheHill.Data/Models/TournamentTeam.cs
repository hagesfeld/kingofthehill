﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Data
{
    public partial class TournamentTeam
    {
        public ObjectId Id { get; set; }
        public string Region { get; set; }
        public int Seed { get; set; }
        public Team Team { get; set; }
        public Player Player { get; set; }
    }
}
