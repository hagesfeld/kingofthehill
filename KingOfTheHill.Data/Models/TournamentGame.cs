﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Data
{
    public partial class TournamentGame
    {
        public ObjectId Id { get; set; }
        public TournamentTeam TeamOne { get; set; }
        public TournamentTeam TeamTwo { get; set; }
        public decimal Spread { get; set; }
        public int TeamOneScore { get; set; }
        public int TeamTwoScore { get; set; }
    }
}
