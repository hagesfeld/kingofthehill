﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Data
{
    public partial class Player
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
    }
}
