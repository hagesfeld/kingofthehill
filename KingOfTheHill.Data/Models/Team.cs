﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Data
{
    public partial class Team
    {
        public ObjectId Id { get; set; }
        public string Name { get; set; }
        public string ScoreName { get; set; }
        public string OddsName { get; set; }
    }
}
