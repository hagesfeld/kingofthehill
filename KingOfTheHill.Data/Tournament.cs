﻿using KingOfTheHill.Data.Models;
using MongoDB.Bson;
using MongoDB.Driver;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Data
{
    public partial class Tournament : BaseData
    {

        public void Set(int year, List<TournamentTeam> teams)
        {
            var newTournament = new Tournament
            {
                Year = year,
                TourneyTeams = teams
            };
            var update = Builders<Tournament>.Update.Set(t => t.TourneyTeams, teams);
            Tournaments.FindOneAndUpdate<Tournament>(new BsonDocument { { "Year", year } },update );
        }

        public Tournament GetByYear(int year)
        {
            return Tournaments.Find(t => t.Year == year).First();
        }
    }
}
