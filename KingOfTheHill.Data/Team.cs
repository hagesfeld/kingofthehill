﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Data
{
    public partial class Team : BaseData
    {
        public IEnumerable<Team> GetAll()
        {
            return  Teams.AsQueryable<Team>().OrderBy(t=>t.Name);
        }

        public Team GetByName(string name)
        {
            return Teams.Find(t => t.Name == name).FirstOrDefault();
        }

        public Team GetById(ObjectId id)
        {
            return Teams.Find(t => t.Id == id).First();
        }
    }
}
