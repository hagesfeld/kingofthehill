﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Data
{
    public partial class Player : BaseData
    {
        public void Add(string name)
        {
            Players.InsertOne(new Player { Name = name });
        }

        public List<Player> GetAll()
        {
            return Players.AsQueryable().ToList();
        }

        public Player GetByName(string name)
        {
            return Players.AsQueryable().FirstOrDefault(p => p.Name == name);
        }
    }
}
