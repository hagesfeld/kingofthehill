﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace KingOfTheHill.Data
{
    public partial class TournamentTeam : BaseData
    {
        public void updateTeam(TournamentTeam team)
        {
            TournamentTeams.ReplaceOne<TournamentTeam>(t => t.Id == team.Id,team);
        }
    }
}
