﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Data
{
    public class BaseData
    {
        protected static IMongoClient _client;
        protected static IMongoDatabase _database;
        public static IMongoCollection<Team> Teams;
        public static IMongoCollection<Tournament> Tournaments;
        public static IMongoCollection<Player> Players;
        public static IMongoCollection<TournamentTeam> TournamentTeams;
        public static IMongoCollection<TournamentGame> Games;

        public BaseData()
        {
            _client = new MongoClient("mongodb://mhagesfelddb:XIrUlPo51xswyh@ds113630.mlab.com:13630/kingofthehill");
                _database = _client.GetDatabase("kingofthehill");
            Teams = _database.GetCollection<Team>("Team");
            Tournaments = _database.GetCollection<Tournament>("Tournament");
            Players = _database.GetCollection<Player>("Player");
            Games = _database.GetCollection<TournamentGame>("TournamentGame");
            TournamentTeams = _database.GetCollection<TournamentTeam>("TournamentTeam");
        }

        public int ConnDatabaseCount()
        {
            return _client.ListDatabases().ToList().Count;
        }
        public void InitTeams()
        {
            Teams.DeleteMany(new BsonDocument());
            Teams.InsertMany(new List<Team> {
                new Team { Name="", ScoreName = "", OddsName = ""},
                new Team { Name="AR-Pine Bluff", ScoreName = "AR-Pine Bluff", OddsName = "AR-Pine Bluff"},
new Team { Name="Abil Christian", ScoreName = "Abil Christian", OddsName = "Abil Christian"},
new Team { Name="Air Force", ScoreName = "Air Force", OddsName = "Air Force"},
new Team { Name="Akron", ScoreName = "Akron", OddsName = "Akron"},
new Team { Name="Alabama", ScoreName = "Alabama", OddsName = "Alabama"},
new Team { Name="Alabama A&M", ScoreName = "Alabama A&M", OddsName = "Alabama A&M"},
new Team { Name="Alabama State", ScoreName = "Alabama State", OddsName = "Alabama State"},
new Team { Name="Albany", ScoreName = "Albany", OddsName = "Albany"},
new Team { Name="Alcorn State", ScoreName = "Alcorn State", OddsName = "Alcorn State"},
new Team { Name="American U", ScoreName = "American U", OddsName = "American U"},
new Team { Name="Appalachian St", ScoreName = "Appalachian St", OddsName = "Appalachian St"},
new Team { Name="Arizona", ScoreName = "Arizona", OddsName = "Arizona"},
new Team { Name="Arizona State", ScoreName = "Arizona State", OddsName = "Arizona State"},
new Team { Name="Arkansas", ScoreName = "Arkansas", OddsName = "Arkansas"},
new Team { Name="Arkansas State", ScoreName = "Arkansas State", OddsName = "Arkansas State"},
new Team { Name="Army", ScoreName = "Army", OddsName = "Army"},
new Team { Name="Auburn", ScoreName = "Auburn", OddsName = "Auburn"},
new Team { Name="Austin Peay", ScoreName = "Austin Peay", OddsName = "Austin Peay"},
new Team { Name="BYU", ScoreName = "BYU", OddsName = "BYU"},
new Team { Name="Ball State", ScoreName = "Ball State", OddsName = "Ball State"},
new Team { Name="Baylor", ScoreName = "Baylor", OddsName = "Baylor"},
new Team { Name="Belmont", ScoreName = "Belmont", OddsName = "Belmont"},
new Team { Name="Bethune-Cookman", ScoreName = "Bethune-Cookman", OddsName = "Bethune-Cookman"},
new Team { Name="Binghamton", ScoreName = "Binghamton", OddsName = "Binghamton"},
new Team { Name="Boise State", ScoreName = "Boise State", OddsName = "Boise State"},
new Team { Name="Boston College", ScoreName = "Boston College", OddsName = "Boston College"},
new Team { Name="Boston U", ScoreName = "Boston U", OddsName = "Boston U"},
new Team { Name="Bowling Green", ScoreName = "Bowling Green", OddsName = "Bowling Green"},
new Team { Name="Bradley", ScoreName = "Bradley", OddsName = "Bradley"},
new Team { Name="Brown", ScoreName = "Brown", OddsName = "Brown"},
new Team { Name="Bryant", ScoreName = "Bryant", OddsName = "Bryant"},
new Team { Name="Bucknell", ScoreName = "Bucknell", OddsName = "Bucknell"},
new Team { Name="Buffalo", ScoreName = "Buffalo", OddsName = "Buffalo"},
new Team { Name="Butler", ScoreName = "Butler", OddsName = "Butler"},
new Team { Name="C. Carolina", ScoreName = "C. Carolina", OddsName = "C. Carolina"},
new Team { Name="CSU Bakersfield", ScoreName = "CSU Bakersfield", OddsName = "CSU Bakersfield"},
new Team { Name="CSU Fullerton", ScoreName = "CSU Fullerton", OddsName = "CSU Fullerton"},
new Team { Name="CSU Northridge", ScoreName = "CSU Northridge", OddsName = "CSU Northridge"},
new Team { Name="Cal", ScoreName = "Cal", OddsName = "Cal"},
new Team { Name="Cal Poly", ScoreName = "Cal Poly", OddsName = "Cal Poly"},
new Team { Name="Campbell", ScoreName = "Campbell", OddsName = "Campbell"},
new Team { Name="Canisius", ScoreName = "Canisius", OddsName = "Canisius"},
new Team { Name="Cent Arkansas", ScoreName = "Cent Arkansas", OddsName = "Cent Arkansas"},
new Team { Name="Cent Conn St", ScoreName = "Cent Conn St", OddsName = "Cent Conn St"},
new Team { Name="Cent Michigan", ScoreName = "Cent Michigan", OddsName = "Cent Michigan"},
new Team { Name="Charleston", ScoreName = "Charleston", OddsName = "Charleston"},
new Team { Name="Charleston So", ScoreName = "Charleston So", OddsName = "Charleston So"},
new Team { Name="Charlotte", ScoreName = "Charlotte", OddsName = "Charlotte"},
new Team { Name="Chattanooga", ScoreName = "Chattanooga", OddsName = "Chattanooga"},
new Team { Name="Chicago State", ScoreName = "Chicago State", OddsName = "Chicago State"},
new Team { Name="Cincinnati", ScoreName = "Cincinnati", OddsName = "Cincinnati"},
new Team { Name="Clemson", ScoreName = "Clemson", OddsName = "Clemson"},
new Team { Name="Cleveland State", ScoreName = "Cleveland State", OddsName = "Cleveland State"},
new Team { Name="Colgate", ScoreName = "Colgate", OddsName = "Colgate"},
new Team { Name="Colorado", ScoreName = "Colorado", OddsName = "Colorado"},
new Team { Name="Colorado State", ScoreName = "Colorado State", OddsName = "Colorado State"},
new Team { Name="Columbia", ScoreName = "Columbia", OddsName = "Columbia"},
new Team { Name="Coppin State", ScoreName = "Coppin State", OddsName = "Coppin State"},
new Team { Name="Cornell", ScoreName = "Cornell", OddsName = "Cornell"},
new Team { Name="Creighton", ScoreName = "Creighton", OddsName = "Creighton"},
new Team { Name="Dartmouth", ScoreName = "Dartmouth", OddsName = "Dartmouth"},
new Team { Name="Davidson", ScoreName = "Davidson", OddsName = "Davidson"},
new Team { Name="Dayton", ScoreName = "Dayton", OddsName = "Dayton"},
new Team { Name="DePaul", ScoreName = "DePaul", OddsName = "DePaul"},
new Team { Name="Delaware", ScoreName = "Delaware", OddsName = "Delaware"},
new Team { Name="Delaware State", ScoreName = "Delaware State", OddsName = "Delaware State"},
new Team { Name="Denver", ScoreName = "Denver", OddsName = "Denver"},
new Team { Name="Detroit Mercy", ScoreName = "Detroit Mercy", OddsName = "Detroit Mercy"},
new Team { Name="Drake", ScoreName = "Drake", OddsName = "Drake"},
new Team { Name="Drexel", ScoreName = "Drexel", OddsName = "Drexel"},
new Team { Name="Duke", ScoreName = "Duke", OddsName = "Duke"},
new Team { Name="Duquesne", ScoreName = "Duquesne", OddsName = "Duquesne"},
new Team { Name="E Illinois", ScoreName = "E Illinois", OddsName = "E Illinois"},
new Team { Name="E Kentucky", ScoreName = "E Kentucky", OddsName = "E Kentucky"},
new Team { Name="E Michigan", ScoreName = "E Michigan", OddsName = "E Michigan"},
new Team { Name="E Washington", ScoreName = "E Washington", OddsName = "E Washington"},
new Team { Name="ECU", ScoreName = "ECU", OddsName = "ECU"},
new Team { Name="ETSU", ScoreName = "ETSU", OddsName = "ETSU"},
new Team { Name="Elon", ScoreName = "Elon", OddsName = "Elon"},
new Team { Name="Evansville", ScoreName = "Evansville", OddsName = "Evansville"},
new Team { Name="FAU", ScoreName = "FAU", OddsName = "FAU"},
new Team { Name="FGCU", ScoreName = "FGCU", OddsName = "FGCU"},
new Team { Name="FIU", ScoreName = "FIU", OddsName = "FIU"},
new Team { Name="FSU", ScoreName = "FSU", OddsName = "FSU"},
new Team { Name="Fair Dickinson", ScoreName = "Fair Dickinson", OddsName = "Fair Dickinson"},
new Team { Name="Fairfield", ScoreName = "Fairfield", OddsName = "Fairfield"},
new Team { Name="Florida", ScoreName = "Florida", OddsName = "Florida"},
new Team { Name="Florida A&M", ScoreName = "Florida A&M", OddsName = "Florida A&M"},
new Team { Name="Fordham", ScoreName = "Fordham", OddsName = "Fordham"},
new Team { Name="Fort Wayne", ScoreName = "Fort Wayne", OddsName = "Fort Wayne"},
new Team { Name="Fresno State", ScoreName = "Fresno State", OddsName = "Fresno State"},
new Team { Name="Furman", ScoreName = "Furman", OddsName = "Furman"},
new Team { Name="G Washington", ScoreName = "G Washington", OddsName = "G Washington"},
new Team { Name="Ga Southern", ScoreName = "Ga Southern", OddsName = "Ga Southern"},
new Team { Name="Gardner-Webb", ScoreName = "Gardner-Webb", OddsName = "Gardner-Webb"},
new Team { Name="George Mason", ScoreName = "George Mason", OddsName = "George Mason"},
new Team { Name="Georgetown", ScoreName = "Georgetown", OddsName = "Georgetown"},
new Team { Name="Georgia", ScoreName = "Georgia", OddsName = "Georgia"},
new Team { Name="Georgia State", ScoreName = "Georgia State", OddsName = "Georgia State"},
new Team { Name="Georgia Tech", ScoreName = "Georgia Tech", OddsName = "Georgia Tech"},
new Team { Name="Gonzaga", ScoreName = "Gonzaga", OddsName = "Gonzaga"},
new Team { Name="Grambling", ScoreName = "Grambling", OddsName = "Grambling"},
new Team { Name="Grand Canyon", ScoreName = "Grand Canyon", OddsName = "Grand Canyon"},
new Team { Name="Green Bay", ScoreName = "Green Bay", OddsName = "Green Bay"},
new Team { Name="Hampton", ScoreName = "Hampton", OddsName = "Hampton"},
new Team { Name="Hartford", ScoreName = "Hartford", OddsName = "Hartford"},
new Team { Name="Harvard", ScoreName = "Harvard", OddsName = "Harvard"},
new Team { Name="Hawai'i", ScoreName = "Hawai'i", OddsName = "Hawai'i"},
new Team { Name="High Point", ScoreName = "High Point", OddsName = "High Point"},
new Team { Name="Hofstra", ScoreName = "Hofstra", OddsName = "Hofstra"},
new Team { Name="Holy Cross", ScoreName = "Holy Cross", OddsName = "Holy Cross"},
new Team { Name="Houston", ScoreName = "Houston", OddsName = "Houston"},
new Team { Name="Houston Baptist", ScoreName = "Houston Baptist", OddsName = "Houston Baptist"},
new Team { Name="Howard", ScoreName = "Howard", OddsName = "Howard"},
new Team { Name="IUPUI", ScoreName = "IUPUI", OddsName = "IUPUI"},
new Team { Name="Idaho", ScoreName = "Idaho", OddsName = "Idaho"},
new Team { Name="Idaho State", ScoreName = "Idaho State", OddsName = "Idaho State"},
new Team { Name="Illinois", ScoreName = "Illinois", OddsName = "Illinois"},
new Team { Name="Illinois State", ScoreName = "Illinois State", OddsName = "Illinois State"},
new Team { Name="Incarnate Word", ScoreName = "Incarnate Word", OddsName = "Incarnate Word"},
new Team { Name="Indiana", ScoreName = "Indiana", OddsName = "Indiana"},
new Team { Name="Indiana State", ScoreName = "Indiana State", OddsName = "Indiana State"},
new Team { Name="Iona", ScoreName = "Iona", OddsName = "Iona"},
new Team { Name="Iowa", ScoreName = "Iowa", OddsName = "Iowa"},
new Team { Name="Iowa State", ScoreName = "Iowa State", OddsName = "Iowa State"},
new Team { Name="JMU", ScoreName = "JMU", OddsName = "JMU"},
new Team { Name="Jackson State", ScoreName = "Jackson State", OddsName = "Jackson State"},
new Team { Name="Jacksonville", ScoreName = "Jacksonville", OddsName = "Jacksonville"},
new Team { Name="Jacksonville St", ScoreName = "Jacksonville St", OddsName = "Jacksonville St"},
new Team { Name="Kansas", ScoreName = "Kansas", OddsName = "Kansas"},
new Team { Name="Kansas State", ScoreName = "Kansas State", OddsName = "Kansas State"},
new Team { Name="Kennesaw St", ScoreName = "Kennesaw St", OddsName = "Kennesaw St"},
new Team { Name="Kent State", ScoreName = "Kent State", OddsName = "Kent State"},
new Team { Name="Kentucky", ScoreName = "Kentucky", OddsName = "Kentucky"},
new Team { Name="LA Tech", ScoreName = "LA Tech", OddsName = "LA Tech"},
new Team { Name="LBSU", ScoreName = "LBSU", OddsName = "LBSU"},
new Team { Name="LIU Brooklyn", ScoreName = "LIU Brooklyn", OddsName = "LIU Brooklyn"},
new Team { Name="LSU", ScoreName = "LSU", OddsName = "LSU"},
new Team { Name="La Salle", ScoreName = "La Salle", OddsName = "La Salle"},
new Team { Name="Lafayette", ScoreName = "Lafayette", OddsName = "Lafayette"},
new Team { Name="Lamar", ScoreName = "Lamar", OddsName = "Lamar"},
new Team { Name="Lehigh", ScoreName = "Lehigh", OddsName = "Lehigh"},
new Team { Name="Liberty", ScoreName = "Liberty", OddsName = "Liberty"},
new Team { Name="Lipscomb", ScoreName = "Lipscomb", OddsName = "Lipscomb"},
new Team { Name="Little Rock", ScoreName = "Little Rock", OddsName = "Little Rock"},
new Team { Name="Longwood", ScoreName = "Longwood", OddsName = "Longwood"},
new Team { Name="Louisville", ScoreName = "Louisville", OddsName = "Louisville"},
new Team { Name="Loyola (MD)", ScoreName = "Loyola (MD)", OddsName = "Loyola (MD)"},
new Team { Name="Loyola Mary", ScoreName = "Loyola Mary", OddsName = "Loyola Mary"},
new Team { Name="Loyola-Chicago", ScoreName = "Loyola-Chicago", OddsName = "Loyola-Chicago"},
new Team { Name="MD-E Shore", ScoreName = "MD-E Shore", OddsName = "MD-E Shore"},
new Team { Name="Maine", ScoreName = "Maine", OddsName = "Maine"},
new Team { Name="Manhattan", ScoreName = "Manhattan", OddsName = "Manhattan"},
new Team { Name="Marist", ScoreName = "Marist", OddsName = "Marist"},
new Team { Name="Marquette", ScoreName = "Marquette", OddsName = "Marquette"},
new Team { Name="Marshall", ScoreName = "Marshall", OddsName = "Marshall"},
new Team { Name="Maryland", ScoreName = "Maryland", OddsName = "Maryland"},
new Team { Name="McNeese", ScoreName = "McNeese", OddsName = "McNeese"},
new Team { Name="Memphis", ScoreName = "Memphis", OddsName = "Memphis"},
new Team { Name="Mercer", ScoreName = "Mercer", OddsName = "Mercer"},
new Team { Name="Miami", ScoreName = "Miami", OddsName = "Miami"},
new Team { Name="Miami (OH)", ScoreName = "Miami (OH)", OddsName = "Miami (OH)"},
new Team { Name="Michigan", ScoreName = "Michigan", OddsName = "Michigan"},
new Team { Name="Michigan State", ScoreName = "Michigan State", OddsName = "Michigan State"},
new Team { Name="Mid Tennessee", ScoreName = "Mid Tennessee", OddsName = "Mid Tennessee"},
new Team { Name="Milwaukee", ScoreName = "Milwaukee", OddsName = "Milwaukee"},
new Team { Name="Minnesota", ScoreName = "Minnesota", OddsName = "Minnesota"},
new Team { Name="Miss St", ScoreName = "Miss St", OddsName = "Miss St"},
new Team { Name="Miss Valley St", ScoreName = "Miss Valley St", OddsName = "Miss Valley St"},
new Team { Name="Missouri", ScoreName = "Missouri", OddsName = "Missouri"},
new Team { Name="Missouri State", ScoreName = "Missouri State", OddsName = "Missouri State"},
new Team { Name="Monmouth", ScoreName = "Monmouth", OddsName = "Monmouth"},
new Team { Name="Montana", ScoreName = "Montana", OddsName = "Montana"},
new Team { Name="Montana State", ScoreName = "Montana State", OddsName = "Montana State"},
new Team { Name="Morehead State", ScoreName = "Morehead State", OddsName = "Morehead State"},
new Team { Name="Morgan State", ScoreName = "Morgan State", OddsName = "Morgan State"},
new Team { Name="Mt St Mary's", ScoreName = "Mt St Mary's", OddsName = "Mt St Mary's"},
new Team { Name="Murray State", ScoreName = "Murray State", OddsName = "Murray State"},
new Team { Name="N Arizona", ScoreName = "N Arizona", OddsName = "N Arizona"},
new Team { Name="N Colorado", ScoreName = "N Colorado", OddsName = "N Colorado"},
new Team { Name="N Illinois", ScoreName = "N Illinois", OddsName = "N Illinois"},
new Team { Name="N Kentucky", ScoreName = "N Kentucky", OddsName = "N Kentucky"},
new Team { Name="NC A&T", ScoreName = "NC A&T", OddsName = "NC A&T"},
new Team { Name="NC Central", ScoreName = "NC Central", OddsName = "NC Central"},
new Team { Name="NC State", ScoreName = "NC State", OddsName = "NC State"},
new Team { Name="NJIT", ScoreName = "NJIT", OddsName = "NJIT"},
new Team { Name="Navy", ScoreName = "Navy", OddsName = "Navy"},
new Team { Name="Nebraska", ScoreName = "Nebraska", OddsName = "Nebraska"},
new Team { Name="Nevada", ScoreName = "Nevada", OddsName = "Nevada"},
new Team { Name="New Mexico", ScoreName = "New Mexico", OddsName = "New Mexico"},
new Team { Name="New Mexico St", ScoreName = "New Mexico St", OddsName = "New Mexico St"},
new Team { Name="New Orleans", ScoreName = "New Orleans", OddsName = "New Orleans"},
new Team { Name="Niagara", ScoreName = "Niagara", OddsName = "Niagara"},
new Team { Name="Nicholls", ScoreName = "Nicholls", OddsName = "Nicholls"},
new Team { Name="Norfolk State", ScoreName = "Norfolk State", OddsName = "Norfolk State"},
new Team { Name="North Dakota", ScoreName = "North Dakota", OddsName = "North Dakota"},
new Team { Name="North Dakota St", ScoreName = "North Dakota St", OddsName = "North Dakota St"},
new Team { Name="North Florida", ScoreName = "North Florida", OddsName = "North Florida"},
new Team { Name="North Texas", ScoreName = "North Texas", OddsName = "North Texas"},
new Team { Name="Northeastern", ScoreName = "Northeastern", OddsName = "Northeastern"},
new Team { Name="Northern Iowa", ScoreName = "Northern Iowa", OddsName = "Northern Iowa"},
new Team { Name="Northwestern", ScoreName = "Northwestern", OddsName = "Northwestern"},
new Team { Name="Northwestern St", ScoreName = "Northwestern St", OddsName = "Northwestern St"},
new Team { Name="Notre Dame", ScoreName = "Notre Dame", OddsName = "Notre Dame"},
new Team { Name="OSU", ScoreName = "OSU", OddsName = "OSU"},
new Team { Name="Oakland", ScoreName = "Oakland", OddsName = "Oakland"},
new Team { Name="Ohio", ScoreName = "Ohio", OddsName = "Ohio"},
new Team { Name="Oklahoma", ScoreName = "Oklahoma", OddsName = "Oklahoma"},
new Team { Name="Oklahoma State", ScoreName = "Oklahoma State", OddsName = "Oklahoma State"},
new Team { Name="Old Dominion", ScoreName = "Old Dominion", OddsName = "Old Dominion"},
new Team { Name="Ole Miss", ScoreName = "Ole Miss", OddsName = "Ole Miss"},
new Team { Name="Omaha", ScoreName = "Omaha", OddsName = "Omaha"},
new Team { Name="Oral Roberts", ScoreName = "Oral Roberts", OddsName = "Oral Roberts"},
new Team { Name="Oregon", ScoreName = "Oregon", OddsName = "Oregon"},
new Team { Name="Oregon State", ScoreName = "Oregon State", OddsName = "Oregon State"},
new Team { Name="PV A&M", ScoreName = "PV A&M", OddsName = "PV A&M"},
new Team { Name="Pacific", ScoreName = "Pacific", OddsName = "Pacific"},
new Team { Name="Penn", ScoreName = "Penn", OddsName = "Penn"},
new Team { Name="Penn State", ScoreName = "Penn State", OddsName = "Penn State"},
new Team { Name="Pepperdine", ScoreName = "Pepperdine", OddsName = "Pepperdine"},
new Team { Name="Pitt", ScoreName = "Pitt", OddsName = "Pitt"},
new Team { Name="Portland", ScoreName = "Portland", OddsName = "Portland"},
new Team { Name="Portland State", ScoreName = "Portland State", OddsName = "Portland State"},
new Team { Name="Presbyterian", ScoreName = "Presbyterian", OddsName = "Presbyterian"},
new Team { Name="Princeton", ScoreName = "Princeton", OddsName = "Princeton"},
new Team { Name="Providence", ScoreName = "Providence", OddsName = "Providence"},
new Team { Name="Purdue", ScoreName = "Purdue", OddsName = "Purdue"},
new Team { Name="Quinnipiac", ScoreName = "Quinnipiac", OddsName = "Quinnipiac"},
new Team { Name="Radford", ScoreName = "Radford", OddsName = "Radford"},
new Team { Name="Rice", ScoreName = "Rice", OddsName = "Rice"},
new Team { Name="Richmond", ScoreName = "Richmond", OddsName = "Richmond"},
new Team { Name="Rider", ScoreName = "Rider", OddsName = "Rider"},
new Team { Name="Robert Morris", ScoreName = "Robert Morris", OddsName = "Robert Morris"},
new Team { Name="Rutgers", ScoreName = "Rutgers", OddsName = "Rutgers"},
new Team { Name="S Carolina St", ScoreName = "S Carolina St", OddsName = "S Carolina St"},
new Team { Name="S Illinois", ScoreName = "S Illinois", OddsName = "S Illinois"},
new Team { Name="SE Louisiana", ScoreName = "SE Louisiana", OddsName = "SE Louisiana"},
new Team { Name="SE Missouri St", ScoreName = "SE Missouri St", OddsName = "SE Missouri St"},
new Team { Name="SF Austin", ScoreName = "SF Austin", OddsName = "SF Austin"},
new Team { Name="SIU ED", ScoreName = "SIU ED", OddsName = "SIU ED"},
new Team { Name="SMU", ScoreName = "SMU", OddsName = "SMU"},
new Team { Name="Sacramento St", ScoreName = "Sacramento St", OddsName = "Sacramento St"},
new Team { Name="Sacred Heart", ScoreName = "Sacred Heart", OddsName = "Sacred Heart"},
new Team { Name="Saint Joe's", ScoreName = "Saint Joe's", OddsName = "Saint Joe's"},
new Team { Name="Saint Louis", ScoreName = "Saint Louis", OddsName = "Saint Louis"},
new Team { Name="Saint Mary's", ScoreName = "Saint Mary's", OddsName = "Saint Mary's"},
new Team { Name="Sam Houston", ScoreName = "Sam Houston", OddsName = "Sam Houston"},
new Team { Name="Samford", ScoreName = "Samford", OddsName = "Samford"},
new Team { Name="San Diego", ScoreName = "San Diego", OddsName = "San Diego"},
new Team { Name="San Diego State", ScoreName = "San Diego State", OddsName = "San Diego State"},
new Team { Name="San Francisco", ScoreName = "San Francisco", OddsName = "San Francisco"},
new Team { Name="San Jose State", ScoreName = "San Jose State", OddsName = "San Jose State"},
new Team { Name="Santa Clara", ScoreName = "Santa Clara", OddsName = "Santa Clara"},
new Team { Name="Savannah State", ScoreName = "Savannah State", OddsName = "Savannah State"},
new Team { Name="Seattle", ScoreName = "Seattle", OddsName = "Seattle"},
new Team { Name="Seton Hall", ScoreName = "Seton Hall", OddsName = "Seton Hall"},
new Team { Name="Siena", ScoreName = "Siena", OddsName = "Siena"},
new Team { Name="South Alabama", ScoreName = "South Alabama", OddsName = "South Alabama"},
new Team { Name="South Carolina", ScoreName = "South Carolina", OddsName = "South Carolina"},
new Team { Name="South Dakota", ScoreName = "South Dakota", OddsName = "South Dakota"},
new Team { Name="South Dakota St", ScoreName = "South Dakota St", OddsName = "South Dakota St"},
new Team { Name="Southern", ScoreName = "Southern", OddsName = "Southern"},
new Team { Name="Southern Miss", ScoreName = "Southern Miss", OddsName = "Southern Miss"},
new Team { Name="Southern Utah", ScoreName = "Southern Utah", OddsName = "Southern Utah"},
new Team { Name="St Bonaventure", ScoreName = "St Bonaventure", OddsName = "St Bonaventure"},
new Team { Name="St Francis (BKN)", ScoreName = "St Francis (BKN)", OddsName = "St Francis (BKN)"},
new Team { Name="St Francis (PA)", ScoreName = "St Francis (PA)", OddsName = "St Francis (PA)"},
new Team { Name="St John's", ScoreName = "St John's", OddsName = "St John's"},
new Team { Name="St Peter's", ScoreName = "St Peter's", OddsName = "St Peter's"},
new Team { Name="Stanford", ScoreName = "Stanford", OddsName = "Stanford"},
new Team { Name="Stetson", ScoreName = "Stetson", OddsName = "Stetson"},
new Team { Name="Stony Brook", ScoreName = "Stony Brook", OddsName = "Stony Brook"},
new Team { Name="Syracuse", ScoreName = "Syracuse", OddsName = "Syracuse"},
new Team { Name="TCU", ScoreName = "TCU", OddsName = "TCU"},
new Team { Name="Temple", ScoreName = "Temple", OddsName = "Temple"},
new Team { Name="Tenn Tech", ScoreName = "Tenn Tech", OddsName = "Tenn Tech"},
new Team { Name="Tennessee", ScoreName = "Tennessee", OddsName = "Tennessee"},
new Team { Name="Tennessee St", ScoreName = "Tennessee St", OddsName = "Tennessee St"},
new Team { Name="Texas", ScoreName = "Texas", OddsName = "Texas"},
new Team { Name="Texas A&M", ScoreName = "Texas A&M", OddsName = "Texas A&M"},
new Team { Name="Texas A&M-CC", ScoreName = "Texas A&M-CC", OddsName = "Texas A&M-CC"},
new Team { Name="Texas Southern", ScoreName = "Texas Southern", OddsName = "Texas Southern"},
new Team { Name="Texas State", ScoreName = "Texas State", OddsName = "Texas State"},
new Team { Name="Texas Tech", ScoreName = "Texas Tech", OddsName = "Texas Tech"},
new Team { Name="The Citadel", ScoreName = "The Citadel", OddsName = "The Citadel"},
new Team { Name="Toledo", ScoreName = "Toledo", OddsName = "Toledo"},
new Team { Name="Towson", ScoreName = "Towson", OddsName = "Towson"},
new Team { Name="Troy", ScoreName = "Troy", OddsName = "Troy"},
new Team { Name="Tulane", ScoreName = "Tulane", OddsName = "Tulane"},
new Team { Name="Tulsa", ScoreName = "Tulsa", OddsName = "Tulsa"},
new Team { Name="UAB", ScoreName = "UAB", OddsName = "UAB"},
new Team { Name="UC Davis", ScoreName = "UC Davis", OddsName = "UC Davis"},
new Team { Name="UC Irvine", ScoreName = "UC Irvine", OddsName = "UC Irvine"},
new Team { Name="UC Riverside", ScoreName = "UC Riverside", OddsName = "UC Riverside"},
new Team { Name="UCF", ScoreName = "UCF", OddsName = "UCF"},
new Team { Name="UCLA", ScoreName = "UCLA", OddsName = "UCLA"},
new Team { Name="UCSB", ScoreName = "UCSB", OddsName = "UCSB"},
new Team { Name="UConn", ScoreName = "UConn", OddsName = "UConn"},
new Team { Name="UIC", ScoreName = "UIC", OddsName = "UIC"},
new Team { Name="UL Lafayette", ScoreName = "UL Lafayette", OddsName = "UL Lafayette"},
new Team { Name="UL Monroe", ScoreName = "UL Monroe", OddsName = "UL Monroe"},
new Team { Name="UMBC", ScoreName = "UMBC", OddsName = "UMBC"},
new Team { Name="UMKC", ScoreName = "UMKC", OddsName = "UMKC"},
new Team { Name="UMass", ScoreName = "UMass", OddsName = "UMass"},
new Team { Name="UMass Lowell", ScoreName = "UMass Lowell", OddsName = "UMass Lowell"},
new Team { Name="UNC", ScoreName = "UNC", OddsName = "UNC"},
new Team { Name="UNC Asheville", ScoreName = "UNC Asheville", OddsName = "UNC Asheville"},
new Team { Name="UNC Wilmington", ScoreName = "UNC Wilmington", OddsName = "UNC Wilmington"},
new Team { Name="UNCG", ScoreName = "UNCG", OddsName = "UNCG"},
new Team { Name="UNH", ScoreName = "UNH", OddsName = "UNH"},
new Team { Name="UNLV", ScoreName = "UNLV", OddsName = "UNLV"},
new Team { Name="URI", ScoreName = "URI", OddsName = "URI"},
new Team { Name="USC", ScoreName = "USC", OddsName = "USC"},
new Team { Name="USC Upstate", ScoreName = "USC Upstate", OddsName = "USC Upstate"},
new Team { Name="USF", ScoreName = "USF", OddsName = "USF"},
new Team { Name="UT Arlington", ScoreName = "UT Arlington", OddsName = "UT Arlington"},
new Team { Name="UT Martin", ScoreName = "UT Martin", OddsName = "UT Martin"},
new Team { Name="UT Rio Grande", ScoreName = "UT Rio Grande", OddsName = "UT Rio Grande"},
new Team { Name="UTEP", ScoreName = "UTEP", OddsName = "UTEP"},
new Team { Name="UTSA", ScoreName = "UTSA", OddsName = "UTSA"},
new Team { Name="UVA", ScoreName = "UVA", OddsName = "UVA"},
new Team { Name="Utah", ScoreName = "Utah", OddsName = "Utah"},
new Team { Name="Utah State", ScoreName = "Utah State", OddsName = "Utah State"},
new Team { Name="Utah Valley", ScoreName = "Utah Valley", OddsName = "Utah Valley"},
new Team { Name="VCU", ScoreName = "VCU", OddsName = "VCU"},
new Team { Name="VMI", ScoreName = "VMI", OddsName = "VMI"},
new Team { Name="Valparaiso", ScoreName = "Valparaiso", OddsName = "Valparaiso"},
new Team { Name="Vanderbilt", ScoreName = "Vanderbilt", OddsName = "Vanderbilt"},
new Team { Name="Vermont", ScoreName = "Vermont", OddsName = "Vermont"},
new Team { Name="Villanova", ScoreName = "Villanova", OddsName = "Villanova"},
new Team { Name="Virginia Tech", ScoreName = "Virginia Tech", OddsName = "Virginia Tech"},
new Team { Name="W Carolina", ScoreName = "W Carolina", OddsName = "W Carolina"},
new Team { Name="W Illinois", ScoreName = "W Illinois", OddsName = "W Illinois"},
new Team { Name="W Kentucky", ScoreName = "W Kentucky", OddsName = "W Kentucky"},
new Team { Name="W Michigan", ScoreName = "W Michigan", OddsName = "W Michigan"},
new Team { Name="Wagner", ScoreName = "Wagner", OddsName = "Wagner"},
new Team { Name="Wake Forest", ScoreName = "Wake Forest", OddsName = "Wake Forest"},
new Team { Name="Washington", ScoreName = "Washington", OddsName = "Washington"},
new Team { Name="Washington St", ScoreName = "Washington St", OddsName = "Washington St"},
new Team { Name="Weber State", ScoreName = "Weber State", OddsName = "Weber State"},
new Team { Name="West Virginia", ScoreName = "West Virginia", OddsName = "West Virginia"},
new Team { Name="Wichita State", ScoreName = "Wichita State", OddsName = "Wichita State"},
new Team { Name="William & Mary", ScoreName = "William & Mary", OddsName = "William & Mary"},
new Team { Name="Winthrop", ScoreName = "Winthrop", OddsName = "Winthrop"},
new Team { Name="Wisconsin", ScoreName = "Wisconsin", OddsName = "Wisconsin"},
new Team { Name="Wofford", ScoreName = "Wofford", OddsName = "Wofford"},
new Team { Name="Wright State", ScoreName = "Wright State", OddsName = "Wright State"},
new Team { Name="Wyoming", ScoreName = "Wyoming", OddsName = "Wyoming"},
new Team { Name="Xavier", ScoreName = "Xavier", OddsName = "Xavier"},
new Team { Name="Yale", ScoreName = "Yale", OddsName = "Yale"},
new Team { Name="Youngstown St", ScoreName = "Youngstown St", OddsName = "Youngstown St"}
            });
            
        }

        public void InitPlayers()
        {
            var players = new List<Player>();
            for (int num = 1; num <= 16;num++)
            {
                Players.InsertOne(new Player { Name=string.Format("Player {0}",num.ToString())});
            }
        }

        public void InitTournament(int year)
        {
            var team = new Team();
            var tournamentTeams = new List<TournamentTeam>();
            foreach(var region in new List<string> { "West", "Midwest", "East", "South" })
            {
                for(int seed =1;seed <=16;seed++)
                {
                    tournamentTeams.Add(new TournamentTeam { Region = region, Seed = seed, Team = team.GetByName("") });
                }
            }
            var tournament = new Tournament();
            tournament.Set(year, tournamentTeams);
        }

        public void SetTestTournament()
        {
            var team = new Team();
            var player = new Player();
            var teamOne = new TournamentTeam { Team = team.GetByName("Northwestern"), Region = "South", Seed = 1, Player= player.GetByName("Player 1") };
            var teamTwo = new TournamentTeam { Team = team.GetByName("Wisconsin"), Region = "South", Seed = 16, Player = player.GetByName("Player 2") };
            var teamThree = new TournamentTeam { Team = team.GetByName("Michigan"), Region = "West", Seed = 1, Player = player.GetByName("Player 3") };
            var teamFour = new TournamentTeam { Team = team.GetByName("Minnesota"), Region = "West", Seed = 16, Player = player.GetByName("Player 4") };
            var teamFive = new TournamentTeam { Team = team.GetByName("UConn"), Region = "Midwest", Seed = 1, Player = player.GetByName("Player 5") };
            var teamSix = new TournamentTeam { Team = team.GetByName("Cincinnati"), Region = "Midwest", Seed = 16, Player = player.GetByName("Player 6") };

            var tournamentTeams = new List<TournamentTeam>
            {
                teamOne,
                teamTwo,
                teamThree,
                teamFour,
                teamFive,
                teamSix

            };

            TournamentTeams.InsertMany(tournamentTeams);
            
            var tournament = new Tournament();
            tournament.Set(2017, tournamentTeams);
            Games.DeleteMany(new BsonDocument());
            Games.InsertMany(new List<TournamentGame>{
                new TournamentGame { TeamOne = teamOne, TeamTwo = teamTwo },
                new TournamentGame { TeamOne = teamThree, TeamTwo = teamFour, Spread = -16 },
                new TournamentGame { TeamOne = teamFive, TeamTwo = teamSix }
                });

        }
        public void InitGames()
        {

        }
    }
}
