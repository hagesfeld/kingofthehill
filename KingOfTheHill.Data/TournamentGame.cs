﻿using MongoDB.Driver;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Data
{
    public partial class TournamentGame : BaseData
    {
        public static TournamentGame GetGameByScoreTeams(string teamNameOne, string teamNameTwo)
        {
            return Games.AsQueryable().Where(g => (g.TeamOne.Team.ScoreName.ToUpper() == teamNameOne.ToUpper() && g.TeamTwo.Team.ScoreName.ToUpper() == teamNameTwo.ToUpper()) ||
            (g.TeamTwo.Team.ScoreName.ToUpper() == teamNameOne.ToUpper() && g.TeamOne.Team.ScoreName.ToUpper() == teamNameTwo.ToUpper())).FirstOrDefault();
        }

        public static TournamentGame GetGameByOddsTeams(string teamNameOne, string teamNameTwo)
        {
            return Games.AsQueryable().Where(g => (g.TeamOne.Team.OddsName.ToUpper() == teamNameOne.ToUpper() && g.TeamTwo.Team.OddsName.ToUpper() == teamNameTwo.ToUpper()) ||
            (g.TeamTwo.Team.OddsName.ToUpper() == teamNameOne.ToUpper() && g.TeamOne.Team.OddsName.ToUpper() == teamNameTwo.ToUpper())).FirstOrDefault();
        }

        public static void UpdateGame(TournamentGame game)
        {
            Games.ReplaceOne<TournamentGame>(g=>g.Id==game.Id, game);
        }
    }
}
