﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace KingOfTheHill.Scores
{
    public class Scores
    {

        public class GameScore
        {
            public string TeamOne { get; set; }
            public int ScoreOne { get; set; }
            public string TeamTwo { get; set; }
            public int ScoreTwo { get; set; }
            public bool IsFinal { get; set; }
        }
        private static string GetScoresFeed()
        {
            var client = new WebClient();
            client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
            return client.DownloadString(string.Format("http://www.espn.com/mens-college-basketball/bottomline/scores?date=03012017?x={0}", Guid.NewGuid().ToString()));
            }

        public static IEnumerable<GameScore> GetScores()
        {
            var scoreFeed = GetScoresFeed();
            var returnScores = new List<GameScore>();
            int firstLeftPos = scoreFeed.IndexOf("s_left");
            int secondLeftPos = 0;
            var nextScore = new GameScore();
            while(firstLeftPos > 0)
            {
                secondLeftPos = scoreFeed.IndexOf("s_left", firstLeftPos+6);
                nextScore = GetScore(scoreFeed.Substring(firstLeftPos+6,secondLeftPos-(firstLeftPos+6)));
                if(nextScore.ScoreOne > 0) returnScores.Add(nextScore);
                firstLeftPos = scoreFeed.IndexOf("s_left", secondLeftPos+6);
            }

            return returnScores;
        }
        private static GameScore GetScore(string scoreLine)
        {
            
            int teamOneStartPos = FindNextLetterPos(scoreLine,0);
            int teamOneEndPos = scoreLine.IndexOf("%20", teamOneStartPos);
            var teamOne = scoreLine.Substring(teamOneStartPos, teamOneEndPos - teamOneStartPos);

            int scoreOne;
            int scoreStartPos = teamOneEndPos + 3;
            int scoreOneEndPos = 0;
            if(!int.TryParse(scoreLine.ElementAt(teamOneEndPos+3).ToString(),out scoreOne))
            {
                return new GameScore { ScoreOne = -1 };
            }

            scoreOneEndPos = scoreLine.IndexOf("%20", scoreStartPos);
            scoreOne = int.Parse(scoreLine.Substring(scoreStartPos, scoreOneEndPos - scoreStartPos));

            var teamTwoStartPos = FindNextLetterPos(scoreLine, scoreOneEndPos);
            var teamTwoEndPos = scoreLine.IndexOf("%20",teamTwoStartPos);
            var nextWordStartPos = FindNextLetterPos(scoreLine, teamTwoEndPos);
            while (nextWordStartPos < scoreLine.IndexOf("(",teamTwoEndPos))
            {
                teamTwoEndPos = scoreLine.IndexOf("%20", nextWordStartPos);
                nextWordStartPos = FindNextLetterPos(scoreLine, teamTwoEndPos);
            }
            var teamTwo = scoreLine.Substring(teamTwoStartPos, teamTwoEndPos - teamTwoStartPos);

            var scoreTwoStartPos = teamTwoEndPos + 3;
            var scoreTwoEndPos = scoreLine.IndexOf("%20", scoreTwoStartPos);
            int scoreTwo = int.Parse(scoreLine.Substring(scoreTwoStartPos, scoreTwoEndPos - scoreTwoStartPos));

            return new GameScore
            {
                 TeamOne = teamOne, TeamTwo = teamTwo,
                 ScoreOne = scoreOne, ScoreTwo = scoreTwo,
                 IsFinal = scoreLine.IndexOf("FINAL") > 0
            };
        }

        private static int FindNextLetterPos(string inLine, int startPos)
        {
            return inLine.Substring(startPos).ToList().FindIndex(c => Char.IsLetter(c)) + startPos;
        }
    }
}
