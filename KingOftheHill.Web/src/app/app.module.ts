import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { TournamentViewComponent } from './components/tournament-view.component';
import { TournamentSetupComponent } from './components/tournament-setup.component';
import { RegionPipe } from './components/tournament-view.component';
import { AppComponent }  from './components/app.component';
@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot([
            {
                path: '',
                redirectTo: '/tournamentdisplay',
                pathMatch: 'full'
            },
            {
                path: 'tournamentsetup',
                component: TournamentSetupComponent
            },
            {
                path: 'tournamentdisplay',
                component: TournamentViewComponent
            }
            
        ])
    ],
    declarations: [
        AppComponent,
        RegionPipe,
        TournamentViewComponent,
        TournamentSetupComponent
    ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }



