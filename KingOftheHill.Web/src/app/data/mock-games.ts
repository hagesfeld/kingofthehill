﻿import { Game } from '../models/game'

export const GAMES: Game[] = [
    { TeamOne: "Ohio State", TeamTwo: "Minnesota", Line: -3 },
    { TeamOne: "Dayton", TeamTwo: "Providence", Line: -10 },
    { TeamOne: "Duke", TeamTwo: "UNC", Line: 6 },
    { TeamOne: "Northwestern", TeamTwo: "Illinois", Line: -5 }
];
