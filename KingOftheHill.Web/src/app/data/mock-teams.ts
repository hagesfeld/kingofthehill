﻿import { Team } from '../models/team'

export const TEAMS: Team[] = [
    { Id: "1", Name: "Ohio State", OddsName: "Ohio State", ScoreName: "Ohio State" },
    { Id: "2", Name: "Dayton", OddsName: "Dayton", ScoreName: "Dayton" },
    { Id: "3", Name: "Kentucky", OddsName: "Kentucky", ScoreName: "Kentucky" },
    { Id: "4", Name: "Duke", OddsName: "Duke", ScoreName: "Duke" },
];
