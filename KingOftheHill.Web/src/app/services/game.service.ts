﻿import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Game } from '../models/game';

@Injectable()
export class GameService {

    private gamesUrl = "https://oddsfeed-dev.calfee.com/api/odds";

    constructor(private http: Http) { }
    returnGames: Game[];

    getGames(): Promise<Game[]> {
        return this.http.get(this.gamesUrl)
            .toPromise()
            .then(response => this.extractGames(response))
            .catch(this.handleError);        
    }

    private extractGames(response: any): Game[] {
        this.returnGames = response.json() as Game[];
        return this.returnGames;
    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}