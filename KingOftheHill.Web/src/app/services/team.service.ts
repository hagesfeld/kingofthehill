﻿import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Team } from '../models/team';
@Injectable()
export class TeamService {

    private teamUrl = "http://localhost:57405/api/team";

    constructor(private http: Http) { }
    returnTeams: Team[];

    getTeams(): Promise<Team[]> {
        return this.http.get(this.teamUrl)
            .toPromise()
            .then(response => this.extractTeams(response))
            .catch(this.handleError);        
    }

    private extractTeams(response: any): Team[] {
        this.returnTeams = response.json() as Team[];
        return this.returnTeams;
    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}