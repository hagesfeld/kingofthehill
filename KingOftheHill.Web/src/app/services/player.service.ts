﻿import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Player } from '../models/player';
@Injectable()
export class TeamService {

    private playerUrl = "http://localhost:57405/api/player";

    constructor(private http: Http) { }

    addPlayer(name): Promise {
        return this.http.post(playerUrlm JSON.stringify({name: name}), {headers: this.headers})
        .toPromise()
        .then(res=>res.json().data)
        .catch(this.handleError);
    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}