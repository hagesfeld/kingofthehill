﻿import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Tournament } from '../models/tournament';
@Injectable()
export class TournamentService {

    private tournamentUrl = "http://localhost:57405/api/tournament/2017";

    constructor(private http: Http) { }
    returnTournament: Tournament;

    getTournament(): Promise<Tournament> {
        return this.http.get(this.tournamentUrl)
            .toPromise()
            .then(response => this.convertResponseToTournament(response))
            .catch(this.handleError);        
    }

    private convertResponseToTournament(response: any): Tournament {
        this.returnTournament = response.json() as Tournament;
        return this.returnTournament;
    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}