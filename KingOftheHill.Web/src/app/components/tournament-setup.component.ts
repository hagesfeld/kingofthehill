import { Component, OnInit, Pipe, PipeTransform  } from '@angular/core';
import { Team } from '../models/team';
import { Tournament } from '../models/tournament';
import { TeamService } from '../services/team.service';
import { TournamentService } from '../services/tournament.service';

@Component({
    selector: 'my-tournament-setup',
    templateUrl: '/app/templates/tournamentsetup.template.html',
    providers: [TeamService, TournamentService]
})

export class TournamentSetupComponent implements OnInit  {
    seeds: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
    teams: Team[];
    selectedTeam: Team;
    currentTournament: Tournament;
    constructor(private teamService: TeamService, private tournamentService: TournamentService) { }

    selectTeam(team: Team): void {
        this.selectedTeam = team;
    }

    getTeams(): void {
        this.teamService.getTeams().then(teamList => this.setTeams(teamList));
    }

    setTeams(teams: Team[]) {
        this.teams = teams;
    }


    getTournament(): void {
        this.tournamentService.getTournament().then(tournament => this.setTournament(tournament));
    }

    setTournament(tournament: Tournament) {
        this.currentTournament = tournament;
    }
    
    ngOnInit(): void {
        this.getTeams();
        this.getTournament();
    }
}

@Pipe({ name: 'regionfilter' })
export class RegionPipe {
    transform(value, args) {
        if (!args) {
            return value;
        }
        else if (value) {
            return value.filter(tournamentTeam => {
                return tournamentTeam.Region == args;
            });
        }
    }
}
