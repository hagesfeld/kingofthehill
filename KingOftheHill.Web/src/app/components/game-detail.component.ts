﻿import { Component, Input } from '@angular/core';
import { Game } from '../models/game'

@Component({
    selector: 'my-game-detail',
    template: `<div *ngIf="game">
<label>Team One:</label> <input [(ngModel)]="game.TeamOne" placeholder="Team One"/>
<label>Line:</label> <input [(ngModel)]="game.Line"/>
<label>Team Two:</label> <input [(ngModel)]="game.TeamTwo" placeholder="Team Two"/>
</div>`
})
export class GameDetailComponent {
    @Input()
    game: Game;
}