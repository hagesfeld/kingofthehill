﻿import { Component, Input, Pipe, PipeTransform } from '@angular/core';
import { Tournament } from '../models/tournament';

@Component({
    selector: 'my-tournament-display',
    templateUrl: '/app/templates/tournamentdisplay.template.html',
    pipes: [RegionPipe]
})

export class TournamentViewComponent   {
    @Input()
    currentTournament: Tournament;
}
@Pipe({name: 'regionfilter'})
export class RegionPipe {
  transform(value, args) {
    if (!args) {
      return value;
    } 
    else if (value) {
      return value.filter(tournamentTeam => {
            return tournamentTeam.Region == args;
      });
    }
  }
}

