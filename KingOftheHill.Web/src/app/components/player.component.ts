﻿import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';
import { Player }                from '../models/player';
import { PlayerService }         from '../services/player.service';

@Component({
  moduleId: module.id,
  selector: 'my-players',
  templateUrl: '../templates/player.template.html'
})
export class PlayerComponent implements OnInit {
  players: Player[];
  selectedPlayer: Player;
  constructor(
    private playerService: PlayerService,
    private router: Router) { }
  getPlayers(): void {
    this.playerService
        .getPlayers()
        .then(players => this.players = players);
  }
  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.create(name)
      .then(hero => {
        this.heroes.push(hero);
        this.selectedHero = null;
      });
  }
  delete(hero: Hero): void {
    this.heroService
        .delete(hero.id)
        .then(() => {
          this.heroes = this.heroes.filter(h => h !== hero);
          if (this.selectedHero === hero) { this.selectedHero = null; }
        });
  }
  ngOnInit(): void {
    this.getHeroes();
  }
  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }
  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedHero.id]);
  }
}
