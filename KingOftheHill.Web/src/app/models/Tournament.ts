﻿import { TournamentTeam } from '../models/tournamentteam'
import { Team } from '../models/team'

export class Tournament {
    Id: string;
    Year: number;
    TournamentTeams: TournamentTeam[];
}