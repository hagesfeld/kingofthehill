﻿import { Team } from '../models/team'

export class TournamentTeam {
    Id: string;
    Team: Team;
    Region: string;
    Seed: number;
}