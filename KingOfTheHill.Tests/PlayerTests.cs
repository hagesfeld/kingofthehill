﻿using KingOfTheHill.Data;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Tests
{
    [TestFixture]
    public class PlayerTests
    {
        Player _player = new Player();
        [Test]
        public void InitPlayersAddsPlayers()
        {
            var data = new BaseData();
            data.InitPlayers();
            Assert.AreEqual(16, _player.GetAll().Count());
        }
    }
}
