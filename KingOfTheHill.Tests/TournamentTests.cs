﻿using KingOfTheHill.Data;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Tests
{
    [TestFixture]
    public class TournamentTests
    {
        [Test]
        public void CreatedTournamentExists()
        {
            var tournament = new Tournament();
            var team = new Team();
            tournament.Set(2017, new List<TournamentTeam>
            {
                new TournamentTeam { Region="West",Seed=1, Team=team.GetByName("OSU") },
                new TournamentTeam { Region="Midwest",Seed=1, Team=team.GetByName("Duke") },
                new TournamentTeam { Region="Midwest",Seed=9, Team=team.GetByName("Texas") }
            });

            var fetchedTournament = tournament.GetByYear(2017);

            Assert.AreEqual(3, fetchedTournament.TourneyTeams.Count());
        }
        [Test]
        public void InitializedTournamentHas64Teams()
        {
            var data = new BaseData();
            data.InitTournament(2017);
            var tournament = new Tournament();
            Assert.AreEqual(64,tournament.GetByYear(2017).TourneyTeams.Count());
        }
    }
}
