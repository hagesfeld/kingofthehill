﻿using KingOfTheHill.Data;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Tests
{
    [TestFixture]
    class ConnectionTests
    {
        [Test]
        public void ClientConnects()
        {
            var data = new BaseData();
            Assert.AreEqual(1, data.ConnDatabaseCount());
        }
    }
}
