﻿using KingOfTheHill.Data;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.Tests
{
    [TestFixture]
    public class TeamTests
    {
        private BaseData _data = new BaseData();
        [Test]
        public void DBInitAdds352Teams()
        {
            _data.InitTeams();
            var team = new Team();
            Assert.AreEqual(352, team.GetAll().Count());
        }

        [Test]
        public void DBInitClearsFirst()
        {
            _data.InitTeams();
            _data.InitTeams();
            var team = new Team();
            Assert.AreEqual(352, team.GetAll().Count());
        }

        [Test]
        public void DBInitAddsOSU()
        {
            var team = new Team();
            Assert.AreEqual("OSU", team.GetByName("OSU").Name);
        }
    }
}
