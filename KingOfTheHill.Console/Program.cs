﻿using KingOfTheHill.Data;
using KingOfTheHill.Scores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingOfTheHill.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var data = new BaseData();
            data.SetTestTournament();

            SetLines();
            SetScores();
        }

        private static void SetScores()
        {
            var scores = Scores.Scores.GetScores();
            var tournamentGame = new TournamentGame();
            foreach (var score in scores)
            {

                tournamentGame = TournamentGame.GetGameByScoreTeams(score.TeamOne, score.TeamTwo);
                if (tournamentGame != null)
                {
                    if (tournamentGame.TeamOne.Team.ScoreName.ToUpper() == score.TeamOne.ToUpper())
                    {
                        tournamentGame.TeamOneScore = score.ScoreOne;
                        tournamentGame.TeamTwoScore = score.ScoreTwo;
                    }
                    else
                    {
                        tournamentGame.TeamOneScore = score.ScoreTwo;
                        tournamentGame.TeamTwoScore = score.ScoreOne;
                    }
                    TournamentGame.UpdateGame(tournamentGame);

                    SwitchTeamsIfNecessary(tournamentGame);
                }
                Console.WriteLine(string.Format("{0} {1} - {2} {3} {4}", score.TeamOne, score.ScoreOne, score.TeamTwo, score.ScoreTwo, score.IsFinal ? "FINAL" : "In Progress"));
            }
        }

        private static void SwitchTeamsIfNecessary(TournamentGame tournamentGame)
        {
            int teamNumber = tournamentGame.Spread < 0 ? 1 : 2;
            if (!TeamCovers(tournamentGame, teamNumber)) SwitchPlayers(tournamentGame, teamNumber);
        }

        private static void SwitchPlayers(TournamentGame tournamentGame, int teamNumber)
        {
            var tt = new TournamentTeam();
            if (teamNumber == 1)
            {
                tournamentGame.TeamOne.Player = tournamentGame.TeamTwo.Player;
                tt.updateTeam(tournamentGame.TeamOne);
            }
            else
            {
                tournamentGame.TeamTwo.Player = tournamentGame.TeamOne.Player;
                tt.updateTeam(tournamentGame.TeamTwo);
            }

        }

        private static bool TeamCovers(TournamentGame tournamentGame, int teamNumber)
        {
            int winScore = teamNumber == 1 ? tournamentGame.TeamOneScore : tournamentGame.TeamTwoScore;
            int loseScore = teamNumber == 1 ? tournamentGame.TeamTwoScore : tournamentGame.TeamOneScore;
            return winScore + tournamentGame.Spread > loseScore;
        }

        private static void SetLines()
        {

            var lines = Lines.Lines.GetLines();
            var tournamentGame = new TournamentGame();
            foreach (var line in lines)
            {
                tournamentGame = TournamentGame.GetGameByOddsTeams(line.TeamOne, line.TeamTwo);
                if (tournamentGame != null)
                {
                    tournamentGame.Spread = tournamentGame.TeamOne.Team.OddsName == line.TeamOne ? line.Line : -1 * line.Line;
                    TournamentGame.UpdateGame(tournamentGame);
                }
                Console.WriteLine(string.Format("{0} {1}{2} {3}", line.TeamOne, line.Line > 0 ? "+" : string.Empty, line.Line, line.TeamTwo));
            }
        }
    }
}
